import math

class Clustering:
    def __init__(self):
        self.datapoints = []   # Array to store all input data points
        self.clusterindex = {}  # Dictionary to store cluster index for each data point. Data point position = key
        self.clusters = {}      # Dictionary holding data point index for each cluster id. Cluster id = key

    def append_datapoint(self, points):
        self.datapoints.append([float(points[0]), float(points[1])])

    def get_datapoints(self):
        return self.datapoints

    def get_datapoints_at_index(self, index):
        return self.datapoints[index]

    def set_cluster_index(self, key, value):
        self.clusterindex[key] = value

    def get_cluster_index(self):
        return self.clusterindex

    def get_cluster_at_key(self, key):
        return self.clusters[key]

    def get_clusters(self):
        return self.clusters

    def get_cluster_count(self):
        return len(self.clusters)

    def append_clusters(self, key, value):
        if key not in self.clusters:
            self.clusters[key] = list()
        self.clusters[key].append(value)

    def modify_clusters(self, index, cluster):
        self.clusters[index] = cluster

    def euclidean_distance(self, pointA, pointB):
        distance = math.sqrt(((pointB[1] - pointA[1]) * (pointB[1] - pointA[1])) + ((pointB[0] - pointA[0]) * (pointB[0] - pointA[0])))
        return distance

    def compute_link(self, cluster_i, cluster_j, s):
        distance = self.euclidean_distance(self.get_datapoints_at_index(cluster_i[0]), self.get_datapoints_at_index(cluster_j[0]))
        mean = 0
        # Compute distance between each pair of points from cluster i and j. Maintain the min distance and return it
        for i in cluster_i:
            for j in cluster_j:
                d = self.euclidean_distance(self.get_datapoints_at_index(i), self.get_datapoints_at_index(j))
                if s == 0:
                    # Single link
                    if d < distance:
                        distance = d
                if s == 1:
                    # Complete link
                    if d > distance:
                        distance = d
                if s == 2:
                    # Average link
                    mean += d
        if s == 2:
            distance = mean / (len(cluster_i) * len(cluster_j))
        return distance


    def merge_clusters(self, measure):
        # Copy all indexes from measure [2] to measure [1]
        self.clusters[measure[1]].extend(self.clusters[measure[2]])
        # The key of measure[1] should be set as the value for measure[2]
        for i in self.clusters[measure[2]]:
            self.clusterindex[i] = measure[1]
        self.clusters.pop(measure[2], None)

if __name__ == '__main__':

    clustering = Clustering()

    # Accept input and store locally
    file = open("input5.txt", "r")
    first_line = file.readline()
    first_line_data = first_line.rstrip().split(' ')

    d = int(first_line_data[0]) # of output clusters desired
    c = int(first_line_data[1]) # of similarity measure
    s = int(first_line_data[2]) # of input data points

    # Start the clustering process. Each data point is its own cluster
    for i in range(d):
        data_line = file.readline()
        point = data_line.rstrip().split(' ')
        clustering.append_datapoint(point)
        clustering.append_clusters(i,i)
        clustering.set_cluster_index(i,i)

    # Perform clustering till optimal no of clusters are reached
    while(clustering.get_cluster_count() > c):
        # Iterate through clusters and merge
        measure = [0.0,0,0] #[distance, index-i, index-j]
        sub_cluster = clustering.get_clusters().copy()
        for i in clustering.get_clusters():
            sub_cluster.pop(i)
            for j in sub_cluster:
                if i != j:
                    # Identify which 2 clusters are closest to each other
                    distance = clustering.compute_link(clustering.get_cluster_at_key(i), clustering.get_cluster_at_key(j),s)
                    if measure[0] == 0.0:
                        measure = [distance, i, j]
                    elif distance < measure[0]:
                        measure = [distance, i, j]
        # Merge the 2 closest clusters together
        clustering.merge_clusters(measure)

    # Clustering is now complete.
    cluster_index = clustering.get_cluster_index()
    for i in cluster_index:
        print cluster_index[i]




