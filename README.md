This algorithm, implements the agglomerative hierarchical clustering algorithm. 
It implements three different cluster similarity measures: single link, complete link, and average link.

The test data is geographical. Each of the data points is a 2-D vector, with longitude and latitude as its dimensions.

During the clustering process, we iteratively aggregate the most similar two clusters, until there are  clusters left. For initialization, each data point forms its own cluster.

The similarity of two clusters Ci Cj is determined by a distance measure. This implementation includes the following 3 measures:

Single link:


Complete link:


Average link:


The smaller the distance is, the more similar the two clusters are.

For distance measures, the algorithm uses  Euclidean distance.

Test data is included for validation.
